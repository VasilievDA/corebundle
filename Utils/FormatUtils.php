<?php
namespace Rup\Bundle\CoreBundle\Utils;

use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\DataTransformer\MoneyToLocalizedStringTransformer;

/**
 * Class FormatUtils
 *
 * @package Rup\Bundle\CoreBundle\Utils
 */
class FormatUtils
{
    /**
     * @param int|float $value
     * @param bool      $grouping
     * @param int       $precision number of digits after comma
     * @param int       $divisor
     *
     * @return string
     */
    public static function moneyFormat($value, $grouping = true, $precision = 2, $divisor = 100)
    {
        $moneyTransformer = new MoneyToLocalizedStringTransformer($precision, $grouping, null, $divisor);

        try {
            $value = $moneyTransformer->transform($value);

            return $value;
        } catch (TransformationFailedException $e) {

        }

        return '';
    }
}
