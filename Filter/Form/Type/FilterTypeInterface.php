<?php
namespace Rup\Bundle\CoreBundle\Filter\Form\Type;

/**
 * Interface FilterTypeInterface
 *
 * @package Rup\Bundle\CoreBundle\Filter\Form\Type
 */
interface FilterTypeInterface
{
    /**
     * Returns the full name of filter class
     *
     * @return string
     */
    public function getFilterClass();
}
