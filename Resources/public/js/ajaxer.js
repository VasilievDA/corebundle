function Ajaxer(options)
{
    var self = this;

    this.defaults = {
        url: null,
        data: null,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        processData: true,
        beforeSend: function(xhr) {},
        success: function(data) {
            var json = new JsonResponse(data);

            if (json.status == 'success' && json.type == 'message') {
                self.onSuccess(json.data);
            } else if (json.status == 'success' && json.type == 'html') {
                self.onSuccessHtml(json.data);
            } else if (json.status == 'error' && (json.type == 'message' || json.type == 'html')) {
                self.onError(json.data);
            } else {
                console.log(json);
            }
        },
        complete: function(xhr) {},
        error: function(xhr, ajaxOptions, thrownError) {},
        onSuccess: function(data) {},
        onSuccessHtml: function(data) {},
        onError: function(data) {
            document.flashMessages.addMessage(data);
        }
    };

    init(options);

    function init(options) {
        for (var name in self.defaults) {
            self[name] = (options !== undefined && options[name] !== undefined)
                ? options[name]
                : self.defaults[name];
        }
    }

    this.send();
}

Ajaxer.prototype = {
    send: function() {
        var self = this;

        $.ajax({
            url: self.url,
            data: self.data,
            type: self.type,
            contentType: self.contentType,
            processData: self.processData,
            beforeSend: function (xhr) {
                self.beforeSend(xhr)
            },
            success: function(data) {
                self.success(data);
            },
            complete: function(xhr) {
                self.complete(xhr);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                self.error(xhr, ajaxOptions, thrownError);
            }
        });
    }
};
