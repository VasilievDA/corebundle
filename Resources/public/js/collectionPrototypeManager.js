function CollectionPrototypeManager(prototypeSelector, collectionHolderSelector, lastIndex, options) {

    this.defaults = {
        addButtonSelector: null,
        prototypeHolderText: '__name__'
    };

    this.init(options);

    var self = this;
    this.$prototype = $(prototypeSelector);
    this.$collectionHolder = $(collectionHolderSelector);

    this.lastIndex = lastIndex;

    if (this.addButtonSelector) {
        this.$addButton = $(this.addButtonSelector);
        self.$addButton.on('click', function(e) {
            e.preventDefault();
            var item = self.getCollectionItemToAdd();
            self.appendCollectionItem(item);
        });
    }
}

CollectionPrototypeManager.prototype = {
    init : function(options) {
        for (var name in this.defaults) {
            this[name] = (options !== undefined && options[name] !== undefined)
                ? options[name]
                : this.defaults[name];
        }
    },

    getCollectionItemToAdd: function() {
        var prototype = this.$prototype.html();
        var index = this.lastIndex;
        var replaceRegExp = new RegExp(this.prototypeHolderText, 'g');
        var collectionBox;
        collectionBox = prototype.replace(replaceRegExp, index);

        return collectionBox;
    },

    appendCollectionItem: function(collectionItem) {
        this.$collectionHolder.append(collectionItem);
        this.lastIndex++;
    },

    deleteCollectionBox: function(collectionBox) {
        $(collectionBox).remove();
    },

    getAddedItems: function(selector) {
        if (!selector) {
            selector = "input[type='hidden']";
        }
        var values = [];
        var $valueHolders = this.$collectionHolder.find(selector).each(
            function(index, element) {
                var value = parseInt($(element).val());
                values.push(value);
            }
        );

        return values;
    }

};