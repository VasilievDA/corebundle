<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Rup\Bundle\CoreBundle\Exception\Controller\UndefinedPropertyException;
use Rup\Bundle\CoreBundle\Exception\Controller\UnimplementedMethodException;
use Rup\Bundle\CoreBundle\Model\Paginator\PaginatorInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractEntityCrudController
 *
 * @package Rup\Bundle\CoreBundle\Controller
 */
abstract class AbstractEntityCrudController extends AbstractController implements PaginationControllerInterface
{
    use PaginationControllerTrait;

    /**
     * @var string
     */
    protected $repositoryName;

    /**
     * @var string
     */
    protected $twigFileNamespace;

    /**
     * @var string
     */
    protected $redirectUrlNamespace;

    /**
     * @var string
     */
    protected $permissionGroup;

    /**
     * @return string
     */
    protected function getTwigFileNamespace()
    {
        return $this->twigFileNamespace;
    }

    /**
     * @return string
     */
    protected function getRedirectUrlNamespace()
    {
        return $this->redirectUrlNamespace;
    }

    /**
     * @return string
     *
     * @throws UndefinedPropertyException
     */
    protected function getPermissionGroup()
    {
        return $this->permissionGroup;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request)
    {
        $this->denyAccessUnlessGranted('list');

        $entities   = $this->getRepository()->findAllForPaginator();
        $pagination = $this->getPagination($entities);

        return $this->render(
            $this->getTwigFileNamespace() . ':list.html.twig',
            array(
                'entities'      => $pagination,
                'previousIndex' => $this->getNumberOfPreviousItemsForPagination($pagination)
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function viewAction(Request $request)
    {
        $this->denyAccessUnlessGranted('view');

        $id = $request->get('id');

        $entity = $this->findEntity($id);

        return $this->render(
            $this->getTwigFileNamespace() . ':view.html.twig',
            array(
                'entity' => $entity
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('create');

        $entityName = $this->getRepository()->getClassName();

        $entity = new $entityName();

        $form = $this->createAddForm($entity);

        if ($request->isMethod('POST')) {

            $form->submit($request);

            if ($form->isValid()) {

                $this->addEntity($entity, $request);

                return $this->afterAdd($entity);
            }
        }

        return $this->render(
            $this->getTwigFileNamespace() . ':add.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request)
    {
        $this->denyAccessUnlessGranted('edit');

        $id = $request->get('id');

        $entity = $this->findEntity($id);

        $form = $this->createEditForm($entity);

        if ($request->isMethod('POST')) {

            $form->submit($request);

            if ($form->isValid()) {
                $this->updateEntity($entity, $request);

                return $this->afterEdit($entity);
            }
        }

        return $this->render(
            $this->getTwigFileNamespace() . ':edit.html.twig',
            array(
                'form'   => $form->createView(),
                'entity' => $entity
            )
        );
    }

    /**
     * @param object $entity
     *
     * @return Form
     *
     * @throws UnimplementedMethodException
     */
    protected function createAddForm($entity)
    {
        throw new UnimplementedMethodException(sprintf('Method "%s" should be implemented in class "%s"',
            __METHOD__, __CLASS__));
    }

    /**
     * @param object $entity
     *
     * @return Form
     *
     * @throws UnimplementedMethodException
     */
    protected function createEditForm($entity)
    {
        throw new UnimplementedMethodException(sprintf('Method "%s" should be implemented in class "%s"',
            __METHOD__, __CLASS__));
    }

    /**
     * @param object  $entity
     * @param Request $request
     */
    protected function addEntity($entity, Request $request)
    {
        $this->getManager()->persist($entity);
        $this->getManager()->flush();
    }

    /**
     * @param object  $entity
     * @param Request $request
     */
    protected function updateEntity($entity, Request $request)
    {
        $this->getManager()->flush();
    }

    /**
     * @param object $entity
     */
    protected function deleteEntity($entity)
    {
        $this->getManager()->remove($entity);
        $this->getManager()->flush();
    }

    /**
     * @param object $entity
     *
     * @return RedirectResponse
     */
    protected function afterAdd($entity)
    {
        return $this->redirect(
            $this->generateUrl($this->getRedirectUrlNamespace() . '_view', array('id' => $entity->getId()))
        );
    }

    /**
     * @param object $entity
     *
     * @return RedirectResponse
     */
    protected function afterEdit($entity)
    {
        return $this->redirect(
            $this->generateUrl($this->getRedirectUrlNamespace() . '_view', array('id' => $entity->getId()))
        );
    }

    /**
     * @return EntityRepository|PaginatorInterface
     */
    protected function getRepository()
    {
        return $this->get($this->repositoryName);
    }

    /**
     * {@inheritdoc}
     */
    protected function denyAccessUnlessGranted($attributes, $object = null, $message = 'Access Denied.')
    {
        if (!$this->container->getParameter('controller.grant.check')) {
            return;
        }

        if (!is_array($attributes)) {
            $attributes = array($attributes);
        }

        $prefix = $this->getPermissionGroup();

        foreach ($attributes as $key => $attribute) {
            if ($prefix && strpos($attribute, '.') === false) {
                $attributes[$key] = $prefix . '.' . $attribute;
            }
        }

        parent::denyAccessUnlessGranted($attributes, $object, $message);
    }
}
