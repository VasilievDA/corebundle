<?php
namespace Rup\Bundle\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Serializer;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Rup\Bundle\CoreBundle\Repository\EntityRepository;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\ObjectMergerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class AbstractRestController extends FOSRestController
{
    /**
     * @return EntityRepository
     */
    abstract protected function getRepository();

    /**
     * @ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when there was bad request",
     *         405 = "Returned when method not allowed",
     *         500 = "Returned when there was an error"
     *     }
     * )
     *
     * @param Request $request
     *
     * @return array|View
     */
    public function cgetAction(Request $request)
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when there was bad request",
     *         405 = "Returned when method not allowed",
     *         500 = "Returned when there was an error"
     *     }
     * )
     *
     * @param Request $request
     *
     * @return array|View
     */
    public function getAction(Request $request)
    {
        return $this->findEntity($request);
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         201 = "Returned when entity was created",
     *         204 = "Returned when there was successful update",
     *         400 = "Returned when there was bad request",
     *         405 = "Returned when method not allowed",
     *         500 = "Returned when there was an error"
     *     }
     * )
     *
     * @param Request $request
     *
     * @return array|View
     */
    public function postAction(Request $request)
    {
        $entityClassName = $this->getRepository()->getClassName();

        $requestEntity = $this->getSerializer()->deserialize(
            $request->getContent(),
            $entityClassName,
            $request->get('_format'),
            $this->getDeserializationContext()
        );

        $responseCode = Codes::HTTP_NO_CONTENT;

        try {
            $entity = $this->findEntity($request);
        } catch (NotFoundHttpException $e) {
            $entity = new $entityClassName();
            $responseCode = Codes::HTTP_CREATED;
        }

        $this->merge($entity, $requestEntity);

        switch ($responseCode) {
            case Codes::HTTP_CREATED:
                $this->add($entity);
                break;
            case Codes::HTTP_NO_CONTENT:
                $this->update($entity);
                break;
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->view($entity->getId(), $responseCode);
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         400 = "Returned when there was bad request",
     *         405 = "Returned when method not allowed",
     *         500 = "Returned when there was an error"
     *     }
     * )
     *
     * @param Request $request
     *
     * @return array|View
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->findEntity($request);

        $this->getRepository()->remove($entity);
        $this->getDoctrine()->getManager()->flush();

        return $this->view(null, Codes::HTTP_NO_CONTENT);
    }

    /**
     * @return Serializer $serializer
     */
    protected function getSerializer()
    {
        return $this->get('jms_serializer');
    }

    /**
     * @return ObjectMergerInterface
     */
    protected function getEntityMerger()
    {
        return $this->get('rup_core.object_merger.base_merger');
    }

    /**
     * @param mixed $entity
     */
    protected function add($entity)
    {
        $this->getDoctrine()->getManager()->persist($entity);
    }

    /**
     * @param mixed $entity
     */
    protected function update($entity)
    {
    }

    /**
     * @param $entity
     * @param $mixin
     */
    protected function merge($entity, $mixin)
    {
        $this->getEntityMerger()->merge($entity, $mixin, $this->getDeserializationContext());
    }

    /**
     * @return DeserializationContext
     */
    protected function getDeserializationContext()
    {
        $context = new DeserializationContext();

        if ($groups = $this->getContextGroups()) {
            $context->setGroups($groups);
        }

        return $context;
    }

    /**
     * @return string[]
     */
    protected function getContextGroups()
    {
        return array();
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    protected function findEntity(Request $request)
    {
        if (!$request->get('id')) {
            throw $this->createNotFoundException();
        }

        $entity = $this->getRepository()->find($request->get('id'));

        if (!$entity) {
            throw $this->createNotFoundException();
        }

        return $entity;
    }
}
