<?php
namespace Rup\Bundle\CoreBundle\Controller;

use JMS\Serializer\DeserializationContext;
use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityRepositoryInterface;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\ObjectMergerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class ExternalEntityRestController extends AbstractRestController
{
    /**
     * @return ObjectMergerInterface
     */
    protected function getEntityMerger()
    {
        return $this->get('rup_core.object_merger.external_entity_merger');
    }

    /**
     * @param $entity
     * @param $mixin
     */
    protected function merge($entity, $mixin)
    {
        $this->getEntityMerger()->merge($entity, $mixin, $this->getDeserializationContext());
    }

    /**
     * @param Request $request
     *
     * @return DeserializationContext
     */
    protected function findEntity(Request $request)
    {
        if (!$request->get('id')) {
            throw $this->createNotFoundException();
        }

        /** @var ExternalEntityRepositoryInterface $repository */
        $repository = $this->getRepository();
        $entity = $repository->findOneByExternalId($request->get('id'));

        if (!$entity) {
            throw $this->createNotFoundException();
        }

        return $entity;
    }
}
