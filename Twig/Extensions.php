<?php
namespace Rup\Bundle\CoreBundle\Twig;

use Rup\Bundle\CoreBundle\Utils\FormatUtils;
use Twig_Extension;
use Twig_SimpleFilter;

/**
 * Class Extensions
 *
 * @package Rup\Bundle\CoreBundle\Twig
 */
class Extensions extends Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            'moneyFormat' => new Twig_SimpleFilter('moneyFormat', array($this, 'moneyFormat')),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array();
    }

    /**
     * @param int|float $value
     * @param string    $defaultNullValue
     * @param bool      $grouping
     * @param int       $precision number of digits after comma
     * @param int       $divisor
     *
     * @return string
     */
    public function moneyFormat($value, $defaultNullValue = '', $grouping = true, $precision = 2, $divisor = 100)
    {
        if (!$value) {
            if (!is_int($defaultNullValue)) {
                return $defaultNullValue;
            } else {
                $value = $defaultNullValue;
            }
        }

        return FormatUtils::moneyFormat($value, $grouping, $precision, $divisor);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rup_core_twig_extension';
    }
}
