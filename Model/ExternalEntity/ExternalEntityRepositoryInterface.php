<?php
namespace Rup\Bundle\CoreBundle\Model\ExternalEntity;

/**
 * Interface ExternalEntityRepositoryTrait
 *
 * @package Rup\Bundle\CoreBundle\Model\ExternalEntity
 */
interface ExternalEntityRepositoryInterface
{
    /**
     * @param int $externalId
     *
     * @return ExternalEntityInterface|null
     */
    public function findOneByExternalId($externalId);
}
