<?php
namespace Rup\Bundle\CoreBundle\Model\ManageDate;

use Rup\Bundle\CoreBundle\Model\Published\PublishedInterface;

/**
 * Interface DateInterface
 *
 * @package Rup\Bundle\CoreBundle\Model\ManageDate
 */
interface DateInterface extends PublishedInterface
{
    /**
     * @param int $createDate
     *
     * @return mixed
     */
    public function setCreateDate($createDate);

    /**
     * @return int
     */
    public function getCreateDate();

    /**
     * @param int $publishDate
     *
     * @return mixed
     */
    public function setPublishDate($publishDate);

    /**
     * @return int
     */
    public function getPublishDate();

    /**
     * @param int $cancelPublishDate
     *
     * @return mixed
     */
    public function setCancelPublishDate($cancelPublishDate);

    /**
     * @return int
     */
    public function getCancelPublishDate();
}