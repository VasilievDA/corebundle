<?php
namespace Rup\Bundle\CoreBundle\Model;

use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;
use Rup\Bundle\CoreBundle\Exception\RepositoryException;

/**
 * Interface ManageableInterface
 *
 * @package Rup\Bundle\CoreBundle\Model
 */
interface ManageableInterface
{
    /**
     * Creates an empty object instance.
     *
     * @return mixed
     */
    public function create();

    /**
     * Add an object to the repository
     *
     * @param mixed $object
     * @throws InvalidArgumentException
     * @throws RepositoryException
     */
    public function add($object);

    /**
     * Removes an object from the repository
     *
     * @param mixed $object
     * @throws InvalidArgumentException
     * @throws RepositoryException
     */
    public function remove($object);
} 
