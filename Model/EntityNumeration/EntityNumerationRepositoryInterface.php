<?php
namespace Rup\Bundle\CoreBundle\Model\EntityNumeration;

/**
 * Interface EntityNumerationRepositoryInterface
 *
 * @package Rup\Bundle\CoreBundle\Model\EntityNumeration
 */
interface EntityNumerationRepositoryInterface
{
    /**
     * @param int $entityNumber
     * @param int $timePeriodFrom
     * @param int $timePeriodTo
     *
     * @return array
     */
    public function findByEntityNumberForTimePeriod($entityNumber, $timePeriodFrom, $timePeriodTo);

    /**
     * @return string
     */
    public function getClassName();
}
