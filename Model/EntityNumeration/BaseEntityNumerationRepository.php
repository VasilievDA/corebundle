<?php
namespace Rup\Bundle\CoreBundle\Model\EntityNumeration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Rup\Bundle\CoreBundle\Exception\EntityNumeration\EntityNumerationNotFoundException;

/**
 * Class BaseEntityNumerationRepository
 *
 * @package Rup\Bundle\CoreBundle\Model\EntityNumeration
 */
class BaseEntityNumerationRepository extends EntityRepository
{
    /**
     * @param EntityNumerationInterface $entity
     *
     * @return int
     */
    public function getEntityNumeration(EntityNumerationInterface $entity)
    {
        if (!$entityNumeration = $this->findEntityNumerationByEntity($entity->getEntityAlias())) {
            $entityNumeration = $this->initEntityNumeration($entity);
        }

        return $entityNumeration->getEntityNumber();
    }

    /**
     * @param string $entityName
     *
     * @return BaseEntityNumeration|null
     */
    public function findEntityNumerationByEntity($entityName)
    {
        $queryBuilder = $this->createQueryBuilder('en');

        $queryBuilder->andWhere('en.entityName = :entityName');

        $queryBuilder->setParameter('entityName', $entityName);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }


    /**
     * @param EntityNumerationInterface $entity
     */
    public function incrementEntityNumeration(EntityNumerationInterface $entity)
    {
        $this->transactional(function () use ($entity) {
            /** @var BaseEntityNumeration $entityNumeration */
            $entityNumeration = $this->findEntityNumerationByEntity($entity->getEntityAlias());

            $this->lock($entityNumeration);

            $entityNumeration->setEntityNumber($entityNumeration->getEntityNumber() + 1);
        });
    }

    /**
     * Resets all entity numerations
     */
    public function resetAllEntityNumerations()
    {
        $this->transactional(function () {
            foreach ($this->findAll() as $entityNumeration) {
                $this->lock($entityNumeration);
                $entityNumeration->setEntityNumber(1);
            }
        });
    }

    /**
     * @param string $entityName
     */
    public function resetEntityNumeration($entityName)
    {
        $this->transactional(function () use ($entityName) {
            if (!$entityNumeration = $this->findEntityNumerationByEntity($entityName)) {
                throw new EntityNumerationNotFoundException("There is no entity numeration for entity \"{$entityName}\".");
            }

            $this->lock($entityNumeration);
            $entityNumeration->setEntityNumber(1);
        });
    }

    /**
     * @return array|null
     */
    public function getExistingEntityNames()
    {
        $result = null;

        foreach ($this->findAll() as $entityNumeration) {
            $result[] = $entityNumeration->getEntityName();
        }

        return $result;
    }

    /**
     * @param EntityNumerationInterface $entity
     *
     * @return BaseEntityNumeration
     */
    protected function initEntityNumeration(EntityNumerationInterface $entity)
    {
        $entityName = $entity->getEntityAlias();

        $entityNumeration = $this->createEntity();
        $entityNumeration->setEntityNumber(1);
        $entityNumeration->setEntityName($entityName);

        $this->_em->persist($entityNumeration);
        $this->_em->flush();

        return $entityNumeration;
    }

    /**
     * @param object $entity
     * @param int    $lockMode
     */
    protected function lock($entity, $lockMode = LockMode::PESSIMISTIC_WRITE)
    {
        $this->_em->lock($entity, $lockMode);
    }

    /**
     * @return BaseEntityNumeration
     */
    protected function createEntity()
    {
        return new $this->_entityName;
    }

    /**
     * @param callable $function
     *
     * @return mixed
     */
    protected function transactional($function)
    {
        $transactionIsolationLevel = $this->_em->getConnection()->getTransactionIsolation();

        $this->_em->getConnection()->setTransactionIsolation(Connection::TRANSACTION_SERIALIZABLE);
        $return = $this->_em->transactional($function);
        $this->_em->getConnection()->setTransactionIsolation($transactionIsolationLevel);

        return $return;
    }
}
