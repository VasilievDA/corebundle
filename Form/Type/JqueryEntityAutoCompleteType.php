<?php
namespace Rup\Bundle\CoreBundle\Form\Type;

use Rup\Bundle\CoreBundle\Form\DataTransformer\JqueryAutoComplete\AutoCompleteTransformer;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JqueryEntityAutoCompleteType
 *
 * Available options:
 *
 * 'url' (required)         - route name to request auto complete results <br/>
 * 'class' (required)       - class used for auto complete <br/>
 * 'property' (required)    - property of class used for search and auto complete display <br/>
 * 'id_property' (id)       - property of class used for save value after auto complete value selected <br/>
 * 'map_property' (null)    - property of class used to map data after form submit. Default is null. Can be 'property',
 *                              'id_property' or null. If set null, then value will be mapped to class instance<br/>
 * 'min_length'(2)          - number of chars to start query at autcomplete <br/>
 */
class JqueryEntityAutoCompleteType extends AbstractType
{

    protected $registry;

    protected $session;

    protected $router;

    /**
     * @param RegistryInterface $registry
     * @param Session $session
     * @param Router $router
     */
    public function __construct(RegistryInterface $registry, Session $session, Router $router)
    {
        $this->registry = $registry;
        $this->session = $session;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('url', $options['url']);
        $builder->setAttribute('property', $options['property']);

        if ($options['map_property'] == $options['property'] || $options['map_property'] == $options['id_property'] || $options['map_property'] === null) {
            $transformer = new AutoCompleteTransformer($this->registry->getManager(), $options['class'], $options['model'], $options['property'], $options['id_property'], $options['map_property'], $options['new_entity']);
        } else {
            throw new LogicException("Option 'map_property' should be set to '{$options['id_property']}', '{$options['property']}' or null, but '{$options['map_property']}' is given");
        }

        $builder->addModelTransformer($transformer);

        $builder->add($options['id_property'], 'hidden');
        $builder->add($options['property'], 'text');
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['url'] = $this->router->generate($options['url']);
        $view->vars['property'] = $options['property'];
        $view->vars['id_property'] = $options['id_property'];
        $view->vars['autocomplete_id'] = 'autocomplete_' . $view->vars['id'];
        $view->vars['min_length'] = $options['min_length'];
        $view->vars['exact_select'] = ($options['map_property'] === null);
        $view->vars['parent_field'] = $options['parent_field'];

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setRequired(
            array(
                'url',
                'class',
                'property'
            )
        );

        $resolver->setDefaults(
            array(
                'placeholder' => "",
                'id_property' => 'id',
                'model' => null,
                'map_property' => null,
                'new_entity' => true,
                'compound' => true,
                'min_length' => 2,
                'error_bubbling' => false,
                'parent_field' => null,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return "text";
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'jquery_entity_autocomplete';
    }
}