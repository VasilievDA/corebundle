<?php
namespace Rup\Bundle\CoreBundle\Form;

use Symfony\Component\Form\Form;

/**
 * Interface CrudFormHandlerInterface
 *
 * @package Rup\Bundle\CoreBundle\Form
 */
interface CrudFormHandlerInterface
{
    /**
     * @param object $object
     *
     * @return Form
     */
    public function getAddForm($object);

    /**
     * @param object $object
     *
     * @return Form
     */
    public function getEditForm($object);
} 