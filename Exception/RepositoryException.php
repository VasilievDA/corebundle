<?php
namespace Rup\Bundle\CoreBundle\Exception;

use RuntimeException;
use Exception as BaseException;

/**
 * Class RepositoryException
 *
 * @package Rup\Bundle\CoreBundle\Exception
 */
class RepositoryException extends RuntimeException implements Exception
{
    /**
     * @param BaseException $e
     * @return RepositoryException
     */
    public static function unableToAddEntityToRepository(BaseException $e)
    {
        return new self($e->getMessage(), $e->getCode(), $e->getPrevious());
    }

    /**
     * @param BaseException $e
     * @return RepositoryException
     */
    public static function unableToRemoveEntityFromRepository(BaseException $e)
    {
        return new self($e->getMessage(), $e->getCode(), $e->getPrevious());
    }

    /**
     * @param BaseException $e
     * @return RepositoryException
     */
    public static function unableToFlushEntity(BaseException $e)
    {
        return new self($e->getMessage(), $e->getCode(), $e->getPrevious());
    }
}
