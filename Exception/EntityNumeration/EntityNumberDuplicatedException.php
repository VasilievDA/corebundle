<?php
namespace Rup\Bundle\CoreBundle\Exception\EntityNumeration;

/**
 * Class EntityNumberDuplicatedException
 *
 * @package Rup\Bundle\CoreBundle\Exception\EntityNumeration
 */
class EntityNumberDuplicatedException extends EntityNumerationException
{
}
