<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class AllowableSymbolsValidator
 *
 * @package Rup\Bundle\CoreBundle\Validator\Constraints
 */
class AllowableSymbolsValidator extends Constraints\RegexValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Constraints\Regex) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Regex');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;

        if ($constraint->match xor preg_match($constraint->pattern, $value)) {
            $this->context->addViolation($constraint->message, array(
                '{{ value }}' => $this->formatValue($value),
                '{{ length }}' => implode(', ', $constraint->length)
            ));
        }
    }
}