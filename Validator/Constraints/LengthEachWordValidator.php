<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class LengthEachWordValidator
 *
 * @package Rup\Bundle\CoreBundle\Validator\Constraints
 */
class LengthEachWordValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $lengthWord = $constraint->length + 1;

        if(preg_match('/\b\S{'.$lengthWord.',}\b/u', $value, $matches)) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation(
                    $constraint->message,
                    array(
                        '{{ length }}' => $constraint->length
                    )
                )
                    ->addViolation();
            } else {
                // 2.4 API
                $this->context->addViolation(
                    $constraint->message,
                    array(
                        '{{ length }}' => $constraint->length
                    )
                );
            }
        }
    }

}