<?php
namespace Rup\Bundle\CoreBundle\PDF;

use Rup\Bundle\CoreBundle\HttpFoundation\PDFResponse;
use WhiteOctober\TCPDFBundle\Controller\TCPDFController;

/**
 * Class AbstractPDF
 */
abstract class AbstractPDF
{
    /**
     * @var TCPDFController
     */
    protected $pdfService;

    /**
     * @var PDFWrapper
     */
    protected $pdf;

    /**
     * Document orientation. 'P' for Portrait, 'A' for Album 
     * 
     * @var string
     */
    protected $orientation = 'P';

    /**
     * Units measure of document.
     * Available values 'px', 'pt', 'mm', 'cm', 'in'
     * 
     * @var string
     */
    protected $unit = 'mm';

    /**
     * Pdf format, for example A4 or A5.
     * All available formats listed at \TCPDF::setPageFormat
     * 
     * @var string
     */
    protected $format = 'A4';

    /**
     * TRUE means that the input text is unicode
     *
     * @var boolean
     */
    protected $unicode = true;

    /**
     * Charset encoding (used only when converting back html entities); default is UTF-8
     * 
     * @var string
     */
    protected $encoding = 'UTF-8';

    /**
     * If TRUE reduce the RAM memory usage by caching temporary data on filesystem (slower)
     * 
     * @var bool
     */
    protected $diskCache = false;

    /**
     * If true set the document to PDF/A mode.
     * 
     * @var boolean
     */
    protected $pdfAMode = false;

    /**
     * @param TCPDFController $pdfService
     */
    public function __construct(TCPDFController $pdfService)
    {
        $this->pdfService = $pdfService;
        $this->createPdf();
    }

    /**
     * Creates PDF writer class
     */
    protected function createPdf()
    {
        $this->pdf = $this->pdfService->create($this->orientation, $this->unit, $this->format, $this->unicode, $this->encoding, $this->diskCache, $this->pdfAMode);
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);
        $this->pdf->setFontSubsetting(false);
        $this->pdf->setTextRenderingMode(0, true);
    }

    /**
     * Returns response with pdf content
     *
     * @param string $name
     *
     * @return PDFResponse
     */
    protected function getResponse($name='output.pdf')
    {
        $output = $this->pdf->Output($name, 'S');

        $response = new PdfResponse($output);
        $response->setFileName($name);

        return $response;
    }
}