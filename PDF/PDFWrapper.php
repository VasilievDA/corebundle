<?php
namespace Rup\Bundle\CoreBundle\PDF;

use TCPDF;

/**
 * Class PDFWrapper
 */
class PDFWrapper extends TCPDF
{
    /**
     * @var string
     */
    public $headerData;

    /**
     * @var string
     */
    public $footerData;

    /**
     * @var float
     */
    public $footerMargin = -15;

    public function setHeader() {
        $this->writeHTMLCell(
            $w = 190,
            $h = '',
            $x = '',
            $y = 10,
            $html = $this->headerData,
            $border = 0,
            $ln = 1,
            $fill = false,
            $reseth = true,
            $align = 'L',
            $autopadding = true
        );
    }

    // Page footer
    public function Footer() {
        $this->SetY($this->footerMargin);

        $footerData = '<div style="text-indent: 45px">'.$this->getPage().'/'.$this->getAliasNbPages().'</div>';

        $this->writeHTMLCell(
            $w = 190,
            $h = '',
            $x = '',
            $y = '',
            $html = ($this->footerData) ? $this->footerData : $footerData,
            $border = 'T',
            $ln = 1,
            $fill = false,
            $reseth = true,
            $align = 'C',
            $autopadding = true
        );
    }
} 