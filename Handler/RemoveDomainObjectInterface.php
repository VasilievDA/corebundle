<?php
namespace Rup\Bundle\CoreBundle\Handler;

/**
 * Interface RemoveDomainObjectInterface
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
interface RemoveDomainObjectInterface
{
    /**
     * @param object $entity
     */
    public function remove($entity);
} 