<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\DeserializationContext;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata\JMSSerializerMetadataCatcher;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata\MetadataCatcherInterface;
use Rup\Bundle\CoreBundle\Utils\ArrayCache;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Traversable;

/**
 * Class AbstractMerger
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger
 */
class BaseMerger implements ObjectMergerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var MetadataCatcherInterface
     */
    protected $metadataCatcher;

    /**
     * @var ArrayCache
     */
    protected $cache;

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * @param ObjectManager                $objectManager manager for fetching existing entities
     * @param JMSSerializerMetadataCatcher $metadataCatcher
     */
    public function __construct(
        ObjectManager $objectManager,
        JMSSerializerMetadataCatcher $metadataCatcher
    )
    {
        $this->cache           = new ArrayCache();
        $this->accessor        = PropertyAccess::createPropertyAccessor();
        $this->objectManager   = $objectManager;
        $this->metadataCatcher = $metadataCatcher;
    }

    /**
     * @param mixed $entity  merge destination
     * @param mixed $mixin   admixing entity
     * @param mixed $context options
     */
    public function merge($entity, $mixin, $context = null)
    {
        $this->metadataCatcher->setContext($context);
        $this->doMerge($entity, $mixin);
        $this->cache->clear();
    }

    /**
     * @param mixed $entity
     * @param mixed $mixin
     */
    protected function doMerge($entity, $mixin)
    {
        $properties = $this->metadataCatcher->getProperties($entity);

        foreach ($properties as $property) {
            if ($this->accessor->isReadable($mixin, $property) &&
                $this->accessor->isWritable($entity, $property) &&
                ($value = $this->accessor->getValue($mixin, $property)) !== null
            ) {
                $this->involveValue($entity, $property, $value);
            }
        }
    }

    /**
     * @param Traversable|mixed $entity
     * @param string            $property
     * @param mixed             $value
     */
    protected function involveValue($entity, $property, $value)
    {
        if ($value instanceof Traversable) {
            $collection = array();

            foreach ($value as $item) {
                $collection[] = $this->mergeProperty($item);
            }

            $value = $collection;

        } else if (is_object($value)) {
            $value = $this->mergeProperty($value);
        }

        $this->accessor->setValue($entity, $property, $value);
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    protected function mergeProperty($value)
    {
        if (!$entity = $this->fetchManagedObject($value)) {
            if (!$entity = $this->cache->fetch($value)) {
                $entity = $value;

                $this->cache->push($entity);
            }
        }

        $this->doMerge($entity, $value);

        return $entity;
    }

    /**
     * @param mixed $value
     *
     * @return object
     */
    protected function fetchManagedObject($value)
    {
        if ($value->getId() === null) {
            return null;
        }

        return $this->objectManager->find(get_class($value), $value->getId());
    }
}
