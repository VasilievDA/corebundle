<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger;

use Doctrine\ORM\EntityManager;
use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityInterface;
use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityRepositoryInterface;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata\JMSSerializerMetadataCatcher;
use Rup\Bundle\CoreBundle\Utils\ArrayCache;

/**
 * Class ExternalEntityMerger
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger
 */
class ExternalEntityMerger extends BaseMerger
{
    /**
     * @var JMSSerializerMetadataCatcher
     */
    protected $metadataCatcher;

    /**
     * @param EntityManager                $entityManager
     * @param JMSSerializerMetadataCatcher $metadataCatcher
     */
    public function __construct(
        EntityManager $entityManager,
        JMSSerializerMetadataCatcher $metadataCatcher
    )
    {
        parent::__construct($entityManager, $metadataCatcher);
        $this->cache = new ArrayCache('externalId');
    }

    /**
     * @param ExternalEntityInterface $value
     *
     * @return ExternalEntityInterface
     */
    protected function fetchManagedObject($value)
    {
        /** @var ExternalEntityRepositoryInterface $repository */
        $repository = $this->objectManager->getRepository(get_class($value));

        return $repository->findOneByExternalId($value->getExternalId());
    }
}
